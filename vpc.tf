#
# VPC Resources
#  * VPC
#  * Subnets
#  * Internet Gateway
#  * Route Table
#

resource "aws_vpc" "goapp" {
  cidr_block = "10.0.0.0/16"

  tags = tomap({
    "Name"                                      = "terraform-eks-goapp-node",
    "kubernetes.io/cluster/${var.cluster-name}" = "shared",
  })
}

resource "aws_subnet" "goapp" {
  count = 2

  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "10.0.${count.index}.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.goapp.id

  tags = tomap({
    "Name"                                      = "terraform-eks-node",
    "kubernetes.io/cluster/${var.cluster-name}" = "shared",
  })
}

resource "aws_internet_gateway" "goapp" {
  vpc_id = aws_vpc.goapp.id

  tags = {
    Name = "terraform-eks-gw"
  }
}

resource "aws_route_table" "goapp" {
  vpc_id = aws_vpc.goapp.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.goapp.id
  }
}

resource "aws_route_table_association" "goapp" {
  count = 2

  subnet_id      = aws_subnet.goapp.*.id[count.index]
  route_table_id = aws_route_table.goapp.id
}